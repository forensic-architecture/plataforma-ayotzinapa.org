import React from 'react';

import copy from '../js/data/copy.json';
import VideoPlayer from './VideoPlayer.jsx';

export default class CabinetVideos extends React.Component {

  renderVideoOverview() {
    return (<p>{copy[this.props.language].cabinet.videos.overview}</p>);
  }

  renderVideos() {
    return (Object.values(copy[this.props.language].cabinet.videos.captions).map(video => {
      if (Array.isArray(video.src)) {
        return (
          <div className="section">
            <h2>{video.title}</h2>
            <p>{video.caption}</p>
            {video.src.map(source => {
                return  (<div className="video-responsive">
                    <iframe width="420" height="315" src={source} frameBorder="0" webkitAllowFullScreen mozAllowFullScreen allowFullScreen></iframe>
                </div>);
            })}
          </div>
        );
      }
      return (
        <div className="section">
          <h2>{video.title}</h2>
          <p>{video.caption}</p>
          <div className="video-responsive">
              <iframe width="420" height="315" src={video.src} frameBorder="0" webkitAllowFullScreen mozAllowFullScreen allowFullScreen></iframe>
          </div>
        </div>
      );
    }));
  }

  render() {
    return (
      <div className="cabinet-file-content">
        <h1>{copy[this.props.language].cabinet.terms.videos}</h1>
        <div className="title-separator" />
        <div className="cabinet-body-text">
          {this.renderVideoOverview()}
          {this.renderVideos()}
        </div>
      </div>
    );
  }
}
