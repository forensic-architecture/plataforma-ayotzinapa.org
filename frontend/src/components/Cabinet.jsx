import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import copy from '../js/data/copy.json';
import VideoPlayer from './VideoPlayer.jsx';

// Redux actions
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';

import CabinetCover from './CabinetCover.jsx';
import CabinetProject from './CabinetProject.jsx';
import CabinetMethod from './CabinetMethod.jsx';
import CabinetVideos from './CabinetVideos.jsx';
import CabinetModels from './CabinetModels.jsx';
import CabinetResources from './CabinetResources.jsx';
import CabinetExhibit from './CabinetExhibit.jsx';
import CabinetSocialMedia from './CabinetSocialMedia.jsx';

class Cabinet extends React.Component {

  constructor(props){
      super(props);
      this.state = {
          tabIndex: 0,
          showCover: true,
          showFileList: false,
      }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ui.flags.isCabinet && nextProps.ui.flags.isCabinet !== this.props.ui.flags.isCabinet) {
      const tabIndex = nextProps.ui.components.cabinetFileTab;
      const showCover = !(tabIndex + 1);
      this.setState({
        showCover,
        tabIndex,
      });
    }
  }

  closeCabinet() {
    this.props.actions.closeCabinet();
  }

  toggleLanguage() {
     this.props.actions.toggleLanguage();
  }

  goToDistrict(districtId) {
    if (!this.props.ui.flags.isFetchingDomain) {
      this.props.handleDistrict(districtId);
      this.props.actions.closeCabinet();
    }
  }

  showFiles(tabIndex) {
      this.setState({
        showCover: false,
        showFileList: false,
        tabIndex: tabIndex
      });
  }

  openCover() {
    this.setState({ showCover: true });
  }

  toggleFileList() {
    this.setState({
      showFileList: !this.state.showFileList,
    });
  }

  renderModelCopy() {
    return copy[this.props.app.language].models.map(paragraph => (<p>{paragraph}</p>));
  }

  renderToggleMenu() {
      let classes = 'side-menu-burg side-menu-file-cabinet';
      classes += (this.state.showFileList) ? ' is-active' : '';
      return (
        <button className={classes} onClick={() => this.toggleFileList()}>
          <span/>
        </button>
      );
  }

  renderHeader() {
    return (
      <div className="cabinet-header">
          <div className="header-title" onClick={() => this.openCover()}>
            <p>Ayotzinapa</p><p>{copy[this.props.app.language].cabinet.terms.untertitle}</p>
          </div>
      </div>
    );
  }

  renderCloseCabinetButton() {
    if (!this.props.app.isMobile && !this.props.ui.flags.isFetchingDomain) {
      return (
        <div className="top-action action not-on-mobile">
          <button onClick={() => this.closeCabinet()}>
            <svg x="0px" y="0px" width="28px" height="28px" viewBox="0 0 40 40">
                <line strokeLinejoin="round" x1="14.102" y1="29.93" x2="25.781" y2="33.123"/>
                <polyline strokeLinejoin="round" points="23.734,11.443 25.781,33.123
                38.659,30.479 32.002,10.423 23.734,11.443 "/>
                <path strokeLinejoin="round" d="M23.753,6.609
                c0-2.105-1.706-3.811-3.811-3.811S16.13,4.504,16.13,6.609c0,0.729,0.208,1.407,0.563,1.986h-0.005l3.254,5.605l3.253-5.605H23.19
                C23.545,8.016,23.753,7.338,23.753,6.609z"/>
                <polyline strokeLinejoin="round" points="17,9.134 15.15,8.499 6.647,12.605
                1.842,32.774 14.102,29.93 15.15,8.499 "/>
                <line strokeLinejoin="round" x1="21.906" y1="10.816" x2="23.734" y2="11.443"/>
            </svg>
            <div className="label">{copy[this.props.app.language].cabinet.terms.investigate} &rarr;</div>
          </button>
        </div>
      );
    }
  }

  renderToggleLanguage() {
    return (
      <div className="secondary-action language-toggle" onClick={() => this.toggleLanguage()}>
        {(this.props.app.language === 'es-MX') ? 'In English' : 'En Español'}
      </div>
    );
  }

  renderTabs() {
    const lang = this.props.app.language;
    return (
      <TabList className="cabinet-file-tab-list">
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.about}</Tab>
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.methodology}</Tab>
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.videos}</Tab>
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.models}</Tab>
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.resources}</Tab>
        <Tab className="cabinet-file-tab">{copy[lang].cabinet.terms.exhibition}</Tab>
        {this.renderToggleLanguage()}
      </TabList>
    );
  }

  renderFiles() {
    let areFilesShowing = (!this.state.showFileList) ? 'file-tab-list-off' : '';

    return (
      <div className={`cabinet-files ${areFilesShowing}`}>
        <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.showFiles(tabIndex)}>
          {this.renderTabs()}
          <TabPanel>
            <CabinetProject
              language={this.props.app.language}
            />
          </TabPanel>
          <TabPanel>
            <CabinetMethod
              language={this.props.app.language}
            />
          </TabPanel>
          <TabPanel>
            <CabinetVideos
              language={this.props.app.language}
            />
          </TabPanel>
          <TabPanel>
            <CabinetModels
              language={this.props.app.language}
              isWebGL={this.props.app.isWebGL}
              isFetching={this.props.ui.flags.isFetchingDomain}
              goToDistrict={(id) => this.goToDistrict(id)}
            />
          </TabPanel>
          <TabPanel>
            <CabinetResources
              language={this.props.app.language}
            />
          </TabPanel>
          <TabPanel>
            <CabinetExhibit
              language={this.props.app.language}
            />
          </TabPanel>
          {this.renderCloseCabinetButton()}
        </Tabs>
        {this.renderToggleMenu()}
        {this.renderHeader()}
        {/*<div className='logo-fa'></div>*/}
      </div>
    );
  }

  render() {
    const isHidden = (!this.props.ui.flags.isCabinet) ? 'hidden' : '';

    if (this.state.showCover) {
      return (
        <div className={`cabinet-wrapper cabinet-wrapper-cover ${isHidden}`}>
          <CabinetCover
            app={this.props.app}
            closeCabinet={() => this.closeCabinet()}
            toggleLanguage={() => this.toggleLanguage()}
            showFiles={(tabIndex) => this.showFiles(tabIndex)}
            isMobile={this.props.app.isMobile}
          />
          <CabinetSocialMedia />
        </div>
      );
    }
    return (
      <div className={`cabinet-wrapper cabinet-wrapper-files ${isHidden}`}>
        {this.renderFiles()}
        <CabinetSocialMedia />
      </div>
    );
  }
}


function mapStateToProps(state) {
  const newState = Object.assign({}, {
      app: {
        language: state.app.language,
        isWebGL: state.app.isWebGL
      },
      ui: Object.assign({}, state.ui)
  });
  return newState;
}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Cabinet);
