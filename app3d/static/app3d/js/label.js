class Label {
    constructor( metadata, keyframe ) {
        this.metadata = metadata;
        var splitCoords = this.metadata.coordinates.split(',');
        var position = new THREE.Vector3( splitCoords[0], Math.abs( splitCoords[2] ), -splitCoords[1] );
        var text = this.metadata.text;
        this.keyframe = this.metadata.Keyframe;
        this.id = this.metadata.id;

        this.text = new THREE.TextSprite( {
            textSize: .5,
            redrawInterval: 1,
            roundFontSizeToNearestPowerOfTwo: true,
            material: {
                color: new THREE.Color( 1.0, 1.0, 1.0 ),
                opacity: 1.0,
                transparent: true,
                alphaTest: 0.4
            },
            texture:{
                text: text,
                fontFamily: 'Verdana, Geneva, sans-serif'
            }
        });
        var getRandomArbitrary = ( min, max ) => {
              return Math.random() * (max - min) + min;
        }
        this.text.position.set( position.x, -position.y + getRandomArbitrary( 2.0, 6.0 ), position.z );
        if( this.keyframe !== keyframe ) this.text.visible = false;
    }

    show() {
        if( this.text )
            this.text.visible = true;
    }

    hide() {
        if( this.text )
            this.text.visible = false;
    }

    getKeyframe() {
        return this.keyframe;
    }

    map_range(value, low1, high1, low2, high2) {
        return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
    }

    updateOpacityAndScale( camera ) {
        var minDistance = 10;

        var position = this.text.position.clone();
        var cameraPosition = camera.position.clone();

        // Determince opacity based on distance between label-camera and cameras limits.
        var distance = position.distanceTo( cameraPosition );
        var middle = ( camera.maxDistance - camera.minDistance ) / 2.0;
        var opacity = Math.min( .9, 1.0 - this.map_range( distance, middle / 2, camera.maxDistance, 0.0, 1.0 ) );
        if( opacity < 0.04 ) opacity = 0;
        if( distance < minDistance )
            distance = minDistance;
        // Determine scale based on distance only.
        var scale = Math.max( 3.0, distance * .090 );
        this.updateOpacity( opacity );
        this.updateScale( scale );
    }

    updateOpacity( opacity ) {
        if( this.text )
            this.text.material.opacity = opacity;
    }

    updateScale( scale ) {
        if( this.text )
            this.text.textSize = Math.max( scale*.25, .9 );
    }
}
