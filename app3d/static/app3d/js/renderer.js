const uuid = require('uuid/v4');
export default class Renderer {
    constructor( selectCb ) {
        this.selectCb = selectCb;
        this.renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: false});
        this.renderer.autoClear = false;
        this.renderer.setClearColor( 0, 0, 0 );
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.renderer.sortObjects = true;
        var renderDom = document.getElementsByTagName( 'canvas' );
        if( renderDom.length > 0 ) {
            var parentNode = renderDom[0].parentNode;
            parentNode.removeChild( renderDom[0] );
        }
        this.context = this.renderer.context;
    }

    markPoiAsSelected( poi ) {
        if( this.selectionIndicator ) {
            this.selectedPoi = poi;
            this.showSelectionIndicator();
        }

    }

    setContainer( container ) {
        container.appendChild( this.renderer.domElement );
    }

    addPopGeometry( popGeometry, transparent = false, type ) {
        var geometry = popGeometry.getGeometry();
        geometry.frustumCulled = false;
        if( transparent ) {
            geometry.material.transparent = true;
            geometry.material.depthWrite = false;
            geometry.material.side = THREE.FrontSide;
        }

        switch( type ) {
            case 'ACTOR':
            {
                if( transparent )
                    geometry.name = 'actor_transparent_'+uuid();
                else
                    geometry.name = 'actor_opaque_'+uuid();
                this.actors_scene.add( geometry );
                break;
            }
            case 'STAGE':
            {
                geometry.name = 'stage';
                if( this.currentScene === 'pj' )
                    geometry.material.depthWrite = true;
                this.stage_scene.add( geometry );
                break;
            }
            case 'CONTEXT':
            {
                geometry.name = 'context';
                this.context_scene.add( geometry );
                break;
            }
        }
        this.render();
    }

    resetSelectedPoi() {
        if( this.selectedPoi ) {
            this.hideSelectionIndicator();
            this.selectedPoi = null;
            this.selectCb( [] );
        }
    }

    removePoi( poi ) {
        if( poi ) {
            var geometryToRemove = this.poi_scene.getObjectByName( poi.geometry );
            if( geometryToRemove )
                this.disposeTHREEObject( geometryToRemove );
            poi.text.dispose();
        }
        this.poi_scene.remove( poi.geometry );
        this.poi_scene.remove( poi.text );
    }

    removeActor( mesh ) {
        var objectToRemove = this.actors_scene.getObjectByName( mesh.name );
        if( objectToRemove ) {
            this.disposeTHREEObject( objectToRemove );
        }
        this.actors_scene.remove( mesh );
        this.render();
    }

    addPoi( poi ) {
        this.pois.push( poi );
        this.poi_scene.add( poi.text );
        this.poi_scene.add( poi.geometry );
    }

    addLabel( label ) {
        this.labels.push( label );
        this.labels_scene.add( label.text );
    }

    getContext() {
        return this.context;
    }

    init( modelJson ) {
        // GUI
        this.gui = new dat.GUI();
        var stage_params = { opacity_mesh: 0.4 }
        this.gui_controller = this.gui.add( stage_params, 'opacity_mesh', 0, 1 ).name('Textura').onChange( () => {
            var stage = this.stage_scene.getObjectByName('stage');
            stage.material.uniforms.uAlpha.value = stage_params.opacity_mesh;
            checkShouldDepthWrite( stage );
            this.render();
        });
        this.gui_controller.listen();
        this.gui.open();
        // Current scene
        this.currentScene = modelJson.name;
        // Camera
        this.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.InnerHeight, 1, 10 );
        this.camera.aspect = window.innerWidth / window.innerHeight;

        var w = Math.max( modelJson.bbox.context.max.x - modelJson.bbox.context.min.x, modelJson.bbox.context.max.y - modelJson.bbox.context.min.y );
        var largeness = Math.max( w, modelJson.bbox.context.max.z - modelJson.bbox.context.min.z );
        var alpha = 45;
        var d = ( w/2 ) / Math.tan( degToRad( alpha/2 ) ) + ( modelJson.bbox.context.max.z - modelJson.bbox.context.min.z ) / 2 + 0.5 * w;
        var initialZ = -d / 2;
        var currentZ = d * .5;

        this.camera.position.z = 200;
        this.camera.position.y = 70;
        this.camera.near = 1;// * this.largeness;
        this.camera.far = largeness * 5.0;
        this.camera.updateProjectionMatrix();

        // Scenes
        this.stage_scene = new THREE.Scene();
        this.actors_scene = new THREE.Scene();
        this.context_scene = new THREE.Scene();
        this.poi_scene = new THREE.Scene();
        this.labels_scene = new THREE.Scene();
        this.dome_scene = new THREE.Scene();
        this.dome_scene.fog = new THREE.Fog( new THREE.Color( .1, .1, .1 ) );

        var axis = new THREE.AxisHelper( 5 );
        //this.poi_scene.add( axis );
        // Lights
        var directionalLightTop = new THREE.DirectionalLight( new THREE.Color( .9, .9, .9 ), .2 );
        var ambientHighlight = new THREE.AmbientLight( new THREE.Color( .7, .7, .7 ), 1.0 );
        var ambientDimlight = new THREE.AmbientLight( new THREE.Color( .2, .2, .2 ), .7 );

        // Stage
        this.stage_scene.add( directionalLightTop.clone() );
        this.stage_scene.add( ambientDimlight.clone() );
        // Actors
        this.actors_scene.add( directionalLightTop.clone() );
        var directionalLightSide = new THREE.DirectionalLight( new THREE.Color( 1, 1, 1 ), .4 );
        this.actors_scene.add( directionalLightSide );
        this.actors_scene.add( ambientHighlight.clone() );

        // Context
        var ambientContextLight = new THREE.AmbientLight( new THREE.Color( .3, .3, .33 ), .6 );
        this.context_scene.add( ambientContextLight );

        var directionalLightTopContext = new THREE.DirectionalLight( new THREE.Color( .6, .6, .8 ), .2 );
        this.context_scene.add( directionalLightTopContext );

        // POI's
        var directionalLightTopPois = new THREE.DirectionalLight( new THREE.Color( .4, .4, .4 ), .6 );
        this.poi_scene.add( directionalLightTopPois );
        var directionalLightPoiSide1 = new THREE.DirectionalLight( new THREE.Color( .4, .4, .4 ), 0.67 );
        directionalLightPoiSide1.position.set( 10, -5, 2 );
        this.poi_scene.add( directionalLightPoiSide1 );
        var directionalLightPoiSide2 = new THREE.DirectionalLight( new THREE.Color( .4, .4, .4 ), .6 );
        directionalLightPoiSide2.position.set( -10, 5, -10 );
        this.poi_scene.add( directionalLightPoiSide2 );
        var directionalLightPoiSide3 = new THREE.DirectionalLight( new THREE.Color( .2, .2, .2 ), .6 );
        directionalLightPoiSide3.position.set( 5, 5, -10 );
        this.poi_scene.add( directionalLightPoiSide3 );
        this.poi_scene.add( ambientHighlight.clone() );
        var domeMat = new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, lights: true, dithering:true, color: new THREE.Color( 1, 1, 1 ) } );
        // Dome
        var dome = new THREE.Mesh( new THREE.SphereBufferGeometry( Math.max( 250, largeness ), 60, 18, 0, 2 * Math.PI, 0, Math.PI / 2 )
                                , domeMat );
        dome.position.y = - 10;
        this.dome_scene.add( dome );
        // Floor
        var floorMat = domeMat.clone();
        var floor = new THREE.Mesh( new THREE.PlaneBufferGeometry( Math.max( 610, largeness * 2.0 ), Math.max( 610, largeness * 2.0 ) ), floorMat );
        floor.rotation.x = - Math.PI / 2;
        floor.position.y = -5;
        this.dome_scene.add( floor );

        var domeAmbient = ambientHighlight.clone();
        domeAmbient.intensity = 1.0;
        domeAmbient.color = new THREE.Color( .00, .00, .00 );
        //domeAmbient.color = new THREE.Color( 1, 1, 1 );
        this.dome_scene.add( domeAmbient  );

        // POI's
        this.pois = new Array();
        this.selectedPoi = null;
        var selectedPoiRing = new THREE.RingGeometry( .22, .245, 30 );
        var selectedPoiRingMaterial = new THREE.MeshPhongMaterial({
            color: new THREE.Color( 1.0, 0.0, 0.0 ),
            transparent: true
        });
        this.selectionIndicator = new THREE.Mesh( selectedPoiRing, selectedPoiRingMaterial );
        this.selectionIndicator.visible = false;
        this.poi_scene.add( this.selectionIndicator );

        // Labels
        this.labels = new Array();

        // Mouse controls
        this.controls = new THREE.OrbitControls( this.camera, this.renderer.domElement );
        this.controls.rotateSpeed = .4;
        this.controls.zoomSpeed = .25;
        this.controls.minDistance = 40;
        this.controls.maxDistance = 200;
        this.controls.minPanX = modelJson.bbox.context.min.x * .8;
        this.controls.maxPanX = modelJson.bbox.context.max.x * .8;
        this.controls.minPanZ = modelJson.bbox.context.min.z * .8;
        this.controls.maxPanZ = modelJson.bbox.context.max.z * .8;
        this.camera.minDistance = this.controls.minDistance;
        this.camera.maxDistance = this.controls.maxDistance;

        this.controls.enableZoom = true;
        this.controls.enablePan = true;
        //this.controls.autoRotate = true;

        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.9;

        this.controls.keys = [ 65, 83, 68 ];
        this.controls.minPolarAngle = 0;
        this.controls.maxPolarAngle = Math.PI * 0.48;

        // Picking
        var raycaster = new THREE.Raycaster();

        // Window resize
        this.onWindowResize = ( event ) => {
            this.camera.aspect = window.innerWidth/window.innerHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize( window.innerWidth, window.innerHeight );
            this.render();
        };
        window.addEventListener( 'resize', this.onWindowResize, false );
        // Mouse coords
        var mouse = new THREE.Vector2();
        // Mouse move
        this.onMouseMove = ( event ) => {
            event.preventDefault();
        };
        document.addEventListener( 'mousemove', this.onMouseMove, false );
        var toScreenPos = ( position3d ) => {
            var width = window.innerWidth;
            var height = window.innerHeight;
            var p = new THREE.Vector3( position3d.x, position3d.x, position3d.y );
            var screenPosition = p.project( this.camera );
            screenPosition.x = ( screenPosition.x + 1 ) / 2 * width;
            screenPosition.y = -( screenPosition.y - 1 ) / 2 * height;
            return screenPosition;
        }
        var isVector3Finite = ( vector ) => {
            return isFinite( vector.x ) && isFinite( vector.y ) && isFinite( vector.z );
        }
        // Mouse down
        this.onMouseDown = ( event ) => {
            event.preventDefault();
            mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
            mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
            raycaster.setFromCamera( mouse, this.camera );
            var intersects = raycaster.intersectObjects( this.poi_scene.children, true );
            if( intersects.length > 0 ) {
                for( var i = 0; i < this.pois.length; i++ ) {
                    if( ( intersects[0].object.parent === this.pois[i].geometry )
                            || ( intersects[0].object === this.pois[i].geometry )
                            || ( intersects[0].object === this.pois[i].text )
                            && this.pois[i] !== this.selectedPoi ) {
                        this.selectedPoi = this.pois[i]; // Update the selected the poi.
                        this.selectionIndicator.position.set( this.selectedPoi.getPosition().x, this.selectedPoi.getPosition().y, this.selectedPoi.getPosition().z );
                        this.selectionIndicator.visible = true;
                        if( this.selectCb && isVector3Finite( this.selectedPoi.getPosition() ) ) { // avoid false positives from pois that have not been positionened properly yet.
                            this.selectCb( this.selectedPoi.getMetadata().events );
                        }
                        this.render();
                    }
                }
            }
        };
        document.addEventListener( 'mousedown', this.onMouseDown, false );
        // Mouse up
        this.onMouseUp = ( event ) => {
            event.preventDefault();
        }
        document.addEventListener( 'mouseup', this.onMouseUp, false );

        this.runAnimation = true;

        this.controls.addEventListener( 'change', () => { this.render(); } );

        var animate = () => {
            if( ! this.runAnimation ) return;
            requestAnimationFrame( animate );
            this.render();
            this.controls.update();
        };

        this.render = () =>  {
            // Update the pois
            updatePois();
            // Update the labels
            updateLabels();
            // Update the opacities of context, stage, actors.
            updateOpacities();
            // Clear the buffer.
            this.renderer.clear();
            // Render the dome
            this.renderer.render( this.dome_scene, this.camera );
            this.renderer.clearDepth();
            // Render the context
            this.renderer.render( this.context_scene, this.camera );
            this.renderer.clearDepth();
            // Render the stage
            this.renderer.render( this.stage_scene, this.camera );
            if( this.currentScene !== 'pj' )
                this.renderer.clearDepth();
            // Render actors
            this.renderer.render( this.actors_scene, this.camera );
            // Render pois
            this.renderer.render( this.poi_scene, this.camera );
            // Render labels
            this.renderer.render( this.labels_scene, this.camera );
        };

        var checkShouldDepthWrite = ( mesh ) => {
            if( this.currentScene !== '' && mesh ) {
                if( this.currentScene === 'pj' ) {
                    if( mesh.material.uniforms.uAlpha.value <= 0.1 ) mesh.material.depthWrite = false;
                    else mesh.material.depthWrite = true;
                }
            }
        }

        var updateOpacities = () => {
            var stage = this.stage_scene.getObjectByName( 'stage' );
            if( stage ) {
                checkShouldDepthWrite( stage );
            }
            var distance = this.camera.position.distanceTo( new THREE.Vector3() );
            var solidDistance = 35;
            if( distance < solidDistance ) {
                distance = solidDistance;
            }
            var context = this.context_scene.getObjectByName( 'context' );
            if( context ) {
                //var opacity = Math.min( solidDistance / distance, context.userData.maxAlpha );
                context.material.uniforms.uAlpha.value = 0.6;//opacity;
            }
        }

        var updateSelectedPoiStyle = () => {
            if( this.selectedPoi && this.selectionIndicator ) {
                this.selectionIndicator.material.opacity = Math.max( 0.4, Math.min( 1.0 - this.selectedPoi.geometry.material.opacity, .85 ) );
                var poiScale = this.selectedPoi.geometry.scale.clone();
                this.selectionIndicator.scale.set( poiScale.x, poiScale.y, poiScale.z );
            }
        }

        var updatePois = () => {
            for( var i = 0; i < this.pois.length; i++ ) {
                this.pois[i].geometry.quaternion.copy( this.camera.quaternion ); // Billboard the poi's
                this.pois[i].updateOpacityAndScale( this.camera );
            }
            if( this.selectionIndicator ) {
                this.selectionIndicator.quaternion.copy( this.camera.quaternion ); // Billboard selection indicator.
            }
            updateSelectedPoiStyle();
        }

        var updateLabels = () => {
            for( var i = 0; i < this.labels.length; i++ ) {
                this.labels[i].updateOpacityAndScale( this.camera );
            }
        }

        // Trigger first render.
        this.render();

        //animate();
    }

    disposeTHREEObject( object ) {
        if( object.geometry ) {
            if( object.geometry.attributes !== undefined ) {
                for( var name in object.geometry.attributes ) {
                    var attribute = object.geometry.attributes[ name ];
                    if( typeof( attribute.array ) !== 'undefined' ) {
                        delete attribute.array;
                    }
                }
            }
        }
        if( object.geometry ) object.geometry.dispose();
        if( object.material ) {
            if( object.material.map ) object.material.map.dispose();
            object.material.dispose();
        }
    }

    disposeScene( scene ) {
        scene.traverse( ( object ) => {
            if( object.children.length > 0 ) {
                object.traverse( ( child ) => {
                    this.disposeTHREEObject( child );
                })
            }
            if( object ) this.disposeTHREEObject( object );
        });

        var children = scene.children;
        for( var i = children.length-1; i >= 0; i-- ) {
            scene.remove( children[i] );
        }
    }

    dispose() {
        this.gui.destroy();
        this.disposeScene( this.context_scene );
        this.disposeScene( this.stage_scene );
        this.disposeScene( this.actors_scene );
        this.disposeScene( this.poi_scene );
        this.disposeScene( this.labels_scene );
        this.disposeScene( this.dome_scene );
        if( this.renderer && ( this.renderer.info.memory.geometries || this.renderer.info.memory.programs || this.renderer.info.memory.textures ) ) {
            console.log( this.renderer.info.memory );
        }
        document.removeEventListener('mousedown', this.onMouseDown, false );
        document.removeEventListener('mouseup', this.onMouseUp, false );
        document.removeEventListener('mousemove', this.onMouseMove, false );
        window.removeEventListener('resize', this.onWindowResize, false );
        this.controls.dispose();
        // Stop the animation loop if we are disposing the current scene.
        this.runAnimation = false;
        // The following render call is funny but necessary to properly dispose the scene after the animation has been paused.
        // See https://github.com/mrdoob/three.js/issues/7391
        this.render();
    }
    getSelectedPoi() {
        return this.selectedPoi;
    }
    showSelectionIndicator() {
        if( this.selectionIndicator && this.selectedPoi ) {
            this.selectionIndicator.position.set( this.selectedPoi.getPosition().x, this.selectedPoi.getPosition().y, this.selectedPoi.getPosition().z );
            this.selectionIndicator.visible = true;
        }
        this.render();
    }

    hideSelectionIndicator() {
        if( this.selectionIndicator ) this.selectionIndicator.visible = false;
        this.render();
    }
}
