import '../scss/main.scss';
import sorting from '../js/data/sorting.json';
import translation from '../js/data/translation.json';

import React from 'react';

import TagSubgroup from './TagSubgroup.jsx';

class TagGroup extends React.Component {

  renderHeader() {
      let label = this.props.groupLabel;
      if (this.props.language === 'en-US') {
          label = translation[label] || label;
      }

      return (
        <div className="taggroup-header">
            <h2>{label}</h2>
        </div>
      );
  }

  renderSubgroup() {
    const subgroups = this.props.tagGroups.byId[this.props.groupName].subgroups;
    return subgroups.map((subgroup, index) => {
      const isSubgroupHasNarratives = (this.props.isNarrative && subgroup.tags.some(tag => this.props.narratives[tag.id]))
      if (!this.props.isNarrative || isSubgroupHasNarratives) {
        return (
          <TagSubgroup
              tags={this.props.tags}
              tagSubgroups={this.props.tagSubgroups}
              narratives={this.props.narratives}
              tagFilters={this.props.tagFilters}
              narrativeFilters={this.props.narrativeFilters}
              filter={this.props.filter}
              key={index}
              subgroup={subgroup}
              language={this.props.language}
              isNarrative={this.props.isNarrative}
          />
        );
      }
    });
  }

  renderContent() {
      return (
        <ul className="taggroup-content">
          {this.renderSubgroup()}
        </ul>
      );
  }

  render() {
    const list = this.props.list;
    return (
      <li className="taggroup-wrapper">
        {this.renderHeader()}
        {this.renderContent()}
      </li>
    );
  }
}

export default TagGroup;
