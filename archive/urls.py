from django.conf.urls import url, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()

urlpatterns = [
    # /archive/
    url(r'^$', views.index, name='index'),

    # API
    url(r'^api/', include(router.urls)),

    url(r'^api/data/$', views.EventsList.as_view()),

    url(r'^api/event/(?P<event_id>[0-9]+)/$',
        views.EventDetail.as_view(), name='event'),

    url(r'^api/search/(?P<query>[\w\-\s]+)$', views.SearchList.as_view()),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^map/(?P<z>[0-9]+)/(?P<x>[0-9]+)/(?P<y>[0-9]+)$', views.MappingProxy.as_view())
]
