import '../scss/main.scss';
import Scene from '../../../app3d/static/app3d/js/scene.js'
import { areEqual } from '../js/data/utilities.js';

import React from 'react';

class View3D extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loadingPct: 0 };
    }

    componentDidMount() {
        this.scene = null;
        this.renderer = this.props.renderer;
        this.app_base_uri = this.props.base_uri;
        this.createSceneFromProps( this.props );
        this.shouldDisplayLoader = 'block';
    }

    componentWillUnmount() {
            this.scene.dispose();
            this.scene = null;
            console.log( "Disposing scene." );
    }

    areLocationEventsEqual( a, b ) {
        if( ( a.length === b.length ) ) {
            for( var i = 0; i < a.length; i++ ) {
                if( ! areEqual( a[i].events, b[i].events ) ) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    updateLoadingPctDisplay( updatedPct ) {
        var pct = Math.ceil( updatedPct * 100 );
        this.setState({
            loadingPct: pct
        });
        this.forceUpdate(); // Skip shouldComponentUpdate and trigger render.
    }

    createSceneFromProps( props ) {
            if( this.scene ) this.scene.dispose();
            var scene_base_uri = this.app_base_uri + props.base_district_uri + props.districtName + '/';
            this.scene = new Scene( scene_base_uri, this.renderer, props.locations );
            this.scene.setLoadingCallback( ( pct ) => { this.updateLoadingPctDisplay( pct ); } );
            if( props.selected.length > 0 ) {
                this.scene.updateSelected( props.selected );
            }
    }

    shouldComponentUpdate( nextProps, nextState ) {
        if( this.props.districtId !== nextProps.districtId ) { // If we have a different district id create new scene.
            this.createSceneFromProps( nextProps );
        }
        else if( ( nextProps.locations.length !== this.props.locations.length ) || ( ! this.areLocationEventsEqual( nextProps.locations, this.props.locations ) ) ) { // We have a scene already check if poi's need to be updated.
            this.updatePois( nextProps );
        }

        if( nextProps.selected.length === 0 && this.props.selected.length > 0 ) { // Otherwise If we are about to close the cardstack unselect the currently selected poi.
            this.resetSelected();
        }

        if( nextProps.keyframe !== this.props.keyframe ) {
            this.scene.reload(nextProps.keyframe);
        }

        return false; // Avoid unnecessary re-renders.For now we only need the initial which triggers by default.
    }

    resetSelected() {
        if( this.scene ) this.scene.resetSelected();
    }

    updatePois( nextProps ) {
        if( this.scene ) this.scene.updatePois( nextProps.locations.slice() ); // We are modifying the array so pass a copy instead of a reference.

        if( nextProps.locations.length === 0 && this.props.locations.length > 0 )
            this.props.toggle('TOGGLE_CARDSTACK');
    }

    hidePois() {
        if( this.scene ) this.scene.hidePois();
    }

    render() {
        this.shouldDisplayLoader = `${(this.state.loadingPct !== 100 ) ? 'block' : 'none' }`
        return (
            <div className='scene-wrapper'>
                <div
                id='container'
                ref={ ( container ) => { if( container ) { this.props.renderer.setContainer( container ); } } }
                />
                <p id='loadingText' style={{display: this.shouldDisplayLoader}}>Cargando: { this.state.loadingPct }%</p>
                <div className="back-to-map">
                    <button onClick={ () => { this.props.toggle('TOGGLE_VIEW'); } } >&larr; Volver al mapa</button>
                </div>
            </div>
       );
    }
}

export default View3D;
