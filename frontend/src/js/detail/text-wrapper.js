// From: https://bl.ocks.org/mbostock/7555321

/**
* This function takes an already existent d3 text element and wraps
* the text around based on the number of words and pixels in the boundaries
* @param {string} text: text to wrap around
* @param {number} width: number of pixels to wrap around
*/

export function wrap(text, width) {
  text.each(function() {
    const text = d3.select(this);
    const words = text.text().split(/\s+/).reverse();
    const lineHeight = 1.2; // em;
    const y = text.attr("y");

    let lineNumber = 0;
    let line = [];
    let word;
    let tspan = text
          .text(null)
          .append("tspan")
          .attr("x", 0)
          .attr("y", y);

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));

      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan =
          text.append("tspan")
            .attr("x", 0)
            .attr("y", ++lineNumber * lineHeight + y + "em")
            .text(word);
      }
    }
  });
}
