import { areEqual, isNotNullNorUndefined } from '../data/utilities';
import config from '../../credentials/config.json';
import 'leaflet-polylinedecorator';

export default function (app, ui, select, activateDistrict) {
    let svg, g, defs;
    // Data object to store events
    let coevents = [];
    let coeventPaths;
    let attacks = [];
    let sites = [];
    let routes = [];
    let locations = [];
    let pathsMap = {};
    let routeMarkers = {};
    let districtMarkers = {};
    let selected = [];
    let highlighted;
    let views = Object.assign({}, app.views);

    const narrativeColors = {
      V: ui.colors.VICTIMS,
      M: ui.colors.MILITARY,
      P: ui.colors.POLICE,
      NS: ui.colors.NONSTATE,
      O: ui.colors.OTHER
    };

    const districts = [
        { id: 9, label: 'Juan Alvarez', coordinates: [[-99.536804, 18.358007],[-99.535184, 18.357406],[-99.535881, 18.354911],[-99.537587, 18.355553]]},
        { id: 7, label: 'Palacio de Justicia', coordinates: [[-99.508097, 18.331882],[-99.508976, 18.330119],[-99.506830, 18.329106],[-99.505966, 18.330985]]},
        { id: 8, label: 'Cruce de Santa Teresa', coordinates: [[-99.530514, 18.234073],[-99.530986, 18.230883],[-99.533454, 18.231433],[-99.532928, 18.235122]]}
    ];

    // Map Settings
    const center = [18.33561, -99.53348];
    const maxBoundaries = [[19.902899, -100.386646], [16.26, -97.911966]];
    const zoomLevel = 14;

    // Initialize layer
    const sitesLayer = L.layerGroup();
    const pathLayer = L.layerGroup();
    const coeventLayer = L.layerGroup();

    // Icons for markPoint flags (a yellow ring around a location)
    const eventCircleMarkers = {};

    // Styles for elements in map
    const settingsPathPolyline = { className: 'path-polyline' };
    const settingsCoeventPolyline = { className: 'coevent-path' };
    const settingsCoeventMarker = { className: 'coevent-marker', radius: 15 };
    const settingsSiteLabel = {
        className: 'site-label',
        opacity: 1,
        permanent: true,
        direction: 'top',
    };


  /**
  * Creates a Leaflet map and a tilelayer for the map background
  * @param {string} id: DOM element to create map onto
  * @param {array} center: [lat, long] coordinates the map will be centered on
  * @param {number} zoom: zoom level
  */
    function initBackgroundMap(id, zoom) {
        /* http://bl.ocks.org/sumbera/10463358 */

        const map = L.map(id)
          .setView(center, zoom)
          .setMinZoom(10)
          .setMaxZoom(20)
          .setMaxBounds(maxBoundaries);

        //L.mapbox.accessToken = config.mapbox_accessToken;
        //L.mapbox.tileLayer('mapbox.satellite').addTo(map);
        L.tileLayer('map/{z}/{x}/{y}').addTo(map);

        map.keyboard.disable();
        const pane = d3.select(map.getPanes().overlayPane);
        const boundingClient = d3.select(`#${id}`).node().getBoundingClientRect();
        const width = boundingClient.width;
        const height = boundingClient.height;

        svg = pane.append('svg')
          .attr('class', 'leaflet-svg')
          .attr('width', width)
          .attr('height', height);

        g = svg.append('g');

        svg.insert('defs', 'g')
            .append('marker')
            .attr('id', 'arrow')
            .attr('viewBox', '0 0 6 6')
            .attr('refX', 3)
            .attr('refY', 3)
            .attr('markerWidth', 14)
            .attr('markerHeight', 14)
            .attr('orient', 'auto')
            .append('path')
            .attr('d', 'M0,3v-3l6,3l-6,3z');

        map.on('zoomstart', () => { svg.classed('hide', true); });
        map.on('zoomend', () => { svg.classed('hide', false); });

        return map;
    }

    // Initialize leaflet map and layers for each type of data
    const lMap = initBackgroundMap(ui.dom.map, zoomLevel);

    function projectPoint(location) {
        const latLng = new L.LatLng(location[0], location[1]);
        return lMap.latLngToLayerPoint(latLng);
    }

    function getSVGBoundaries() {
        return {
            topLeft: projectPoint(maxBoundaries[0]),
            bottomRight: projectPoint(maxBoundaries[1])
        }
    }

    function updateSVG() {
        const boundaries = getSVGBoundaries();
        const { topLeft, bottomRight } = boundaries;
        svg.attr('width', bottomRight.x - topLeft.x + 200)
          .attr('height', bottomRight.y - topLeft.y + 200)
          .style('left', `${topLeft.x - 100}px`)
          .style('top', `${topLeft.y - 100}px`);

        g.attr('transform', `translate(${-(topLeft.x - 100)},${-(topLeft.y - 100)})`);

        g.selectAll('.location').attr('transform', (d) => {
            const newPoint = projectPoint([+d.latitude, +d.longitude]);
            return `translate(${newPoint.x},${newPoint.y})`;
        });

        g.selectAll('.attack-marker').attr('transform', (d) => {
            const newPoint = projectPoint([+d.event.location.latitude, +d.event.location.longitude]);
            return `translate(${newPoint.x},${newPoint.y})`;
        });

        const busLine = d3.line()
            .x(d => lMap.latLngToLayerPoint(d).x)
            .y(d => lMap.latLngToLayerPoint(d).y)
            .curve(d3.curveMonotoneX);

        const districtLine = d3.line()
            .x(d => lMap.latLngToLayerPoint(d).x)
            .y(d => lMap.latLngToLayerPoint(d).y)
            .curve(d3.curveLinearClosed);

        g.selectAll('.bus-route')
            .attr('d', d => busLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))));

        g.selectAll('.bus-route-marker')
            .attr('d', d => busLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))));

        g.selectAll('.district')
            .attr('d', d => districtLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))));
    }

    lMap.on("zoom viewreset move", updateSVG);

    /**
    * Returns latitud / longitude
    * @param {Object} eventPoint: data for an evenPoint - time, loc, tags, etc
    */
    function getEventLocation(eventPoint) {
        return {
            latitude: +eventPoint.location.latitude,
            longitude: +eventPoint.location.longitude,
        };
    }

    /**
     * Returns a key for the narrative group, as it should be colored
     * @param {object} narrative: tag object for the narrative
     */
    function getNarrativeColorGroup(narrative) {
        const group = narrative.group;
        const subgroup = narrative.subgroup;
        if (group === 'Víctimas') return 'V';
        if (group === 'Fuerzas de seguridad' && subgroup === 'Ejército') return 'M';
        if (group === 'Fuerzas de seguridad' && subgroup !== 'Ejército') return 'P';
        if (group === 'Presuntos miembros del crimen organizado') return 'NS';
        return 'O';
    }

    /*
     * INTERACTIVE FUNCTIONS
     */

    /**
     * Removes the circular ring to mark a particular location
     */
    function unmarkPoint() {
        Object.keys(eventCircleMarkers).forEach(markerId => {
          lMap.removeLayer(eventCircleMarkers[markerId]);
          delete eventCircleMarkers[markerId];
        });
    }

    /**
     * Makes a circular ring mark in one particular location at a time
     * @param {object} location object, with lat and long
     */
    function renderSelected() {
        unmarkPoint();

        selected.forEach(eventPoint => {
          if (isNotNullNorUndefined(eventPoint) && isNotNullNorUndefined(eventPoint.location)) {
              const { latitude, longitude } = getEventLocation(eventPoint);
              if (latitude && longitude) {
                  const location = new L.LatLng(latitude, longitude);
                  eventCircleMarkers[eventPoint.id] = L.circleMarker(location, {
                      radius: 32,
                      fill: false,
                      color: '#ffffff',
                      weight: 3,
                      lineCap: '',
                      dashArray: '5,2'
                  });
                  eventCircleMarkers[eventPoint.id].addTo(lMap);
              }
          }
        })
    }

    function renderHighlighted() {
        const eventPoint = highlighted;
        if (isNotNullNorUndefined(eventPoint) && isNotNullNorUndefined(eventPoint.location)) {
            const { latitude, longitude } = getEventLocation(eventPoint);
            if (latitude && longitude) {
                const location = new L.LatLng(latitude, longitude);
                lMap.flyTo(location);
            }
        }
    }

    /*
     * RENDERING FUNCTIONS
     */

     function getLocationEventsDistribution(location) {
         const eventsHere = { V: 0, M: 0, P: 0, NS: 0, O: 0 };

         location.events.forEach((event) => {
             const groupKey = getNarrativeColorGroup(event.narrative);
             eventsHere[groupKey] += 1;
         });

         eventsHere.O += eventsHere.V + eventsHere.P + eventsHere.M + eventsHere.NS;
         eventsHere.NS += eventsHere.V + eventsHere.P + eventsHere.M;
         eventsHere.M += eventsHere.V + eventsHere.P;
         eventsHere.P += eventsHere.V;

         return [eventsHere.O, eventsHere.NS, eventsHere.M, eventsHere.P, eventsHere.V]
     }

    /**
    * Clears existing event layer
    * Renders all events as markers
    * Adds eventlayer to map
    */
    function renderEvents() {
        const colors = [narrativeColors.O, narrativeColors.NS, narrativeColors.M,
            narrativeColors.P, narrativeColors.V];

        const locationsDom = g.selectAll('.location')
          .data(locations, d => d.id)

        locationsDom
          .exit()
          .remove();

        locationsDom
          .enter().append('g')
          .attr('class', 'location')
          .attr('transform', (d) => {
            d.LatLng = new L.LatLng(+d.latitude, +d.longitude);
              return `translate(${lMap.latLngToLayerPoint(d.LatLng).x},
                  ${lMap.latLngToLayerPoint(d.LatLng).y})`;
          })
          .on('click', (location) => { select(location.events); });

        const eventsDom = g.selectAll('.location').selectAll('.location-event-marker')
          .data((d, i) => getLocationEventsDistribution(locations[i]),
            (d, i) => 'location-' + i);

        eventsDom
          .exit()
          .attr('r', 0)
          .remove();

        eventsDom
          .transition()
          .duration(500)
          .attr('r', d => (d) ? Math.sqrt(16 * d) + 3 : 0);

        eventsDom
          .enter().append('circle')
          .attr('class', 'location-event-marker')
          .style('fill', (d, i) => colors[i])
          .transition()
          .duration(500)
          .attr('r', d => (d) ? Math.sqrt(16 * d) + 3 : 0);
    }

    /**
    * Renders a circle related to an important attack area
    */
    function renderAttacks() {
        const attacksDom = g
          .selectAll('.attack-marker')
          .data(attacks, d => d.event.id)

        attacksDom
          .exit()
          .transition()
          .duration(500)
          .attr('r', 0)
          .remove();

        attacksDom
          .enter()
          .append('circle')
          .attr('class', 'attack-marker')
          .attr('transform', (d) => {
            d.LatLng = new L.LatLng(+d.event.location.latitude, +d.event.location.longitude);
              return `translate(${lMap.latLngToLayerPoint(d.LatLng).x},
                  ${lMap.latLngToLayerPoint(d.LatLng).y})`;
          })
          .on('click', (attack) => { select([+attack.event.id]); })
          .transition()
          .duration(500)
          .attr('r', 40);
    }

    function renderDistricts() {
      const districtLine = d3.line()
          .x(d => lMap.latLngToLayerPoint(d).x)
          .y(d => lMap.latLngToLayerPoint(d).y)
          .curve(d3.curveLinearClosed);

      districts.forEach((district) => {
          const button = L.DomUtil.create('button');
          button.innerHTML = `<p>Ir a:</p><p>${district.label}</p>`;
          const popup = L.popup({ className: 'district-popup do-display', offset: [0, -10] }).setContent(button);
          L.DomEvent.on(button, 'click', () => activateDistrict(district.id));

          const latlng = [district.coordinates[0][0], district.coordinates[0][1]];

          // Create an invisible marker for each route
          let marker;
          if (districtMarkers[district.id]) {
              marker = districtMarkers[district.id];
              marker.bindPopup(popup);
              lMap.addLayer(marker);
          } else {
              marker = L.circleMarker(latlng, { radius: 0, stroke: 0, pane: 'tooltipPane' });
              marker.bindPopup(popup);
              lMap.addLayer(marker);
              districtMarkers[district.id] = marker;
          }

          const linePath = g.selectAll(`.district-${district.id}`)
              .data([district])
              .enter().append('path')
              .attr('class', `district-${district.id} district`);

          linePath
              .attr('d', d => districtLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))))
              .style('stroke', ui.colors.VICTIMS)
              .style('stroke-width', 2)
              .style('fill', ui.colors.VICTIMS)
              .style('fill-opacity', 0.2)
              .style('cursor', 'pointer')
              .on('click', function (d, i) {
                  d3.event.stopPropagation();
                  const pos = [d3.mouse(this)[0], d3.mouse(this)[1] - 10];
                  marker.setLatLng(lMap.layerPointToLatLng(L.point(pos))).openPopup();
              });
        });
    }


    function renderSites() {
        sitesLayer.clearLayers();
        lMap.removeLayer(sitesLayer);

        // Create a label for each attack site, persistent across filtering
        if (views.sites) {
            sites.forEach((site) => {
                if (isNotNullNorUndefined(site) && isNotNullNorUndefined(site.location)) {
                    const { latitude, longitude } = getEventLocation(site);

                    // Create an invisible marker for each site label
                    const siteMarker = L.circleMarker([latitude, longitude], { radius: 0, stroke: 0 });

                    siteMarker.bindTooltip(site.name, settingsSiteLabel).openTooltip();

                    // Add this one attack marker to group attack layer
                    sitesLayer.addLayer(siteMarker);
                }
            });

            lMap.addLayer(sitesLayer);
        }
    }

    /**
     * Renders a black thick line for a bus route
     */
    function renderBusRoutes() {
        if (views.routes) {
          const toLine = d3.line()
              .x(d => lMap.latLngToLayerPoint(d).x)
              .y(d => lMap.latLngToLayerPoint(d).y)
              .curve(d3.curveMonotoneX);

          routes.forEach((route) => {
              const popup = L.popup(
                  { className: 'do-display' },
                  { offset: L.point(0, 10) }
                ).setContent(route.label);

              const latlng = [route.coordinates[0][1], route.coordinates[0][0]];

                // Create an invisible marker for each route
              let marker;
              if (routeMarkers[route.id]) {
                  marker = routeMarkers[route.id];
                  marker.bindPopup(popup);
                  lMap.addLayer(marker);
              } else {
                  marker = L.circleMarker(latlng, { radius: 0, stroke: 0, pane: 'tooltipPane' });
                  marker.bindPopup(popup);
                  lMap.addLayer(marker);
                  routeMarkers[route.id] = marker;
              }

              const linePath = g.selectAll(`.bus-route-${route.id}`)
                  .data([route])
                  .enter().append('path')
                  .attr('class', `bus-route-${route.id} bus-route`);

              const markerPath = g.selectAll(`.bus-route-${route.id}-marker`)
                  .data([route])
                  .enter().append('path')
                  .attr('class', `bus-route-${route.id}-marker bus-route-marker`);

              markerPath
                  .attr('marker-end', 'url(#arrow)')
                  .attr('mid-marker', 'url(#arrow)')
                  .attr('d', d => toLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))))
                  .style('fill', 'none')
                  .style('stroke', 'none');

              linePath
                  .attr('d', d => toLine(d.coordinates.map(c => new L.LatLng(+c[1], +c[0]))))
                  .attr('stroke', ui.colors.DARKGREY)
                  .attr('stroke-width', route.weight * 1.5 + 2)
                  .attr('fill', 'none')
                  .style('cursor', 'pointer')
                  .on('click', function (d, i) {
                      d3.event.stopPropagation();
                      const pos = [d3.mouse(this)[0], d3.mouse(this)[1] - 10];
                      marker.setLatLng(lMap.layerPointToLatLng(L.point(pos))).openPopup();
                  });
          });
        } else {
          g.selectAll('.bus-route')
              .data([])
              .exit().remove();

          g.selectAll('.bus-route-marker')
              .data([])
              .exit().remove();
        }
    }

    /**
     * Creates a marker for a location with coeventstyles
     * @param {object} loc: location in eventPoint location form
     */
    function createCoeventMarker(loc, narrative) {
        const color = narrativeColors[getNarrativeColorGroup(narrative)];
        const settings = Object.assign({}, settingsCoeventMarker, {
            color: color,
            fill: color,
        });
        if (loc) return L.circleMarker([loc.latitude, loc.longitude], settings);
        return false;
    }

    /**
     * Creates a polyline between two locations
     * @param {object} loc1: location in eventPoint location form
     * @param {object} loc2: location in eventPoint location form
     */
    function createCoeventLine(loc1, loc2, narrative) {
        const color = narrativeColors[getNarrativeColorGroup(narrative)];
        const settings = Object.assign({}, settingsCoeventPolyline, { color });
        return L.polyline([
                [+loc1.latitude, +loc1.longitude],
                [+loc2.latitude, +loc2.longitude],
        ], settings);
    }

    /**
     * Add transition to a coevent (communication) line
     * @param (object) coeventPath: d3 selection of the path to animate
     */
    function coeventTransition(coeventPath) {
        coeventPath
            .attr('stroke-dashoffset', 0)
            .transition()
            .duration(6000)
            .ease(d3.easeLinear)
            .attr('stroke-dashoffset', 80)
            .on('end', () => coeventTransition(coeventPath));
    }

    /**
     * Creats a marker for an eventPoint along a path
     * @param {Object} coevMarker: a leaflet marker for coevent eventPoint 1
     * @param {Object} coevMarker2: a leaflet marker for coevent eventPoint 2
     * @param {Object} line: a leaflet polyline between the events
     */
    function unhighlightCoevent(coevMarker, coevMarker2, line, color) {
        const settings = Object.assign({}, settingsCoeventPolyline, { color });
        coevMarker.setStyle({ fillOpacity: 0.1 });
        coevMarker2.setStyle({ fillOpacity: 0.1 });
        line.setStyle(settings);
    }

    /**
     * Creats a marker for an eventPoint along a path
     * @param {Object} coevMarker: a leaflet marker for coevent eventPoint 1
     * @param {Object} coevMarker2: a leaflet marker for coevent eventPoint 2
     * @param {Object} line: a leaflet polyline between the events
     */
    function highlightCoevent(coevMarker, coevMarker2, line) {
        coevMarker.setStyle({ fillOpacity: 0.4 });
        coevMarker2.setStyle({ fillOpacity: 0.4 });
        line.setStyle({ dashArray: null });
    }

    /**
     * Renders coevents, displays a dotted line between connected events
     * Loads all coevents
     */
    function renderCoevents() {
        coeventLayer.clearLayers();
        lMap.removeLayer(coeventLayer);

        if (views.coevents) {
            coevents.forEach((coev) => {
                const locTrans = coev.location_transmitter;
                const locRec = coev.location_receiver;
                const narrative = coev.event.narrative;
                const color = narrativeColors[getNarrativeColorGroup(narrative)];
                const settingsPolyline = Object.assign({}, settingsCoeventPolyline, { color });

                if (isNotNullNorUndefined(locTrans) && isNotNullNorUndefined(locRec)) {
                    // Create markers for event 1 and 2
                    // + create polyline for communication
                    const coevMarker = createCoeventMarker(locTrans, narrative);
                    const coevMarker2 = createCoeventMarker(locRec, narrative);
                    const polyline = createCoeventLine(locTrans, locRec, narrative);

                    polyline.on('mouseover', () => { polyline.setStyle({ weight: '5px' }); });
                    polyline.on('mouseout', () => { polyline.setStyle(settingsPolyline); });

                    const coeventGroup = L.featureGroup([coevMarker, coevMarker2, polyline])
                        .on('mouseover', () => { highlightCoevent(coevMarker, coevMarker2, polyline); })
                        .on('mouseout', () => { unhighlightCoevent(coevMarker, coevMarker2, polyline, color); });

                    coeventGroup
                        .on('click', () => { select([coev.event]); });

                    // Add all to layer
                    coeventGroup.addTo(coeventLayer);
                }
            });

            lMap.addLayer(coeventLayer);
            coeventPaths = d3.selectAll('.coevent-path');
            coeventPaths
                .each(function () {
                    coeventTransition(d3.select(this));
                });
        }
    }

    /**
     * Creats a marker for an eventPoint along a path
     * @param {Object} eventPoint: data for an evenPoint - time, loc, tags, etc
     * @param {number} step: the portion of the entire path this event corresponds to
     */
    function createPathEventMarker(eventPoint, step) {
        const { latitude, longitude } = getEventLocation(eventPoint);
        const pathEventMarker = L.circleMarker(
                  [latitude, longitude], {
                      color: ui.colors.DARKGREY,
                      fill: ui.colors.DARKGREY,
                      weight: 2,
                      fillOpacity: 0.6,
                      radius: 10 * step,
                  },
              );

        // Add marker event handlers
        pathEventMarker.bindPopup('');
        pathEventMarker.on('popupopen', () => { select([eventPoint]); });
        pathEventMarker.on('popupclose', () => { select(); });

        return pathEventMarker;
    }

    /**
     * Renders all active paths and their eventPoints along the way
     */
    function renderPaths() {
        // Clean slate for paths
        pathLayer.clearLayers();
        lMap.removeLayer(pathLayer);
        pathsMap = {};

        // Add a point per step of the path
        pathsAllIds.forEach((pathId) => {
            const path = paths[pathId];
            const pathEventPoints = [];
            const singlePathLayer = L.layerGroup();

            path.forEach((eventPoint, index) => {
                if (isNotNullNorUndefined(eventPoint.location)) {
                    // which step of all trajectory (or the number of eventpoints)
                    const step = (index + 1) / path.length;
                    // Create a marker at each eventpoint in the path
                    const pathEventMarker = createPathEventMarker(eventPoint, step);
                    singlePathLayer.addLayer(pathEventMarker);

                    if (isNotNullNorUndefined(eventPoint.timestamp)) {
                        const { latitude, longitude } = getEventLocation(eventPoint);
                        pathEventPoints.push([latitude, longitude]);
                    }
                }
            });

            // Create polyline connecting events along the path
            const polyline = L.polyline(pathEventPoints, settingsPathPolyline);
            singlePathLayer.addLayer(polyline);

            // Add path to map to use in toggler
            pathsMap[pathId] = singlePathLayer;
            pathLayer.addLayer(singlePathLayer);
        });

        // if (views.paths) lMap.addLayer(pathLayer);
        lMap.addLayer(pathLayer);
    }

  /**
  * Switches on a path, given a path id
  * @param {string} pathId: id of the path to be turned on (corresponds to a narrative id)
  */
  function turnPathOn(pathId) {
    const pathMapLayer = pathsMap[pathId];
    pathLayer.addLayer(pathMapLayer);
  }

  /**
  * Switches off a path, given a path id
  * @param {string} pathId: id of the path to be turned on (corresponds to a narrative id)
  */
  function turnPathOff(pathId) {
    const pathMapLayer = pathsMap[pathId];
    if (pathMapLayer) {
      pathLayer.removeLayer(pathMapLayer);
    }
  }

    /**
     * Updates displayable data on the map: events, coevents and paths
     * @param {Object} domain: object of arrays of events, coevs, attacks, paths, sites
     */
    function update(domain, app) {
        locations = domain.locations;
        coevents = (app.views.coevents) ? domain.coevents : [];
        attacks = domain.attacks;
        routes = (app.views.routes) ? domain.routes : [];
        sites = domain.sites;

        selected = app.selected;
        highlighted = app.highlighted;
        views = app.views;
        updateSVG();
    }

     /**
      * Renders events on the map: takes data, and enters, updates and exits
      */
    function render() {
        renderBusRoutes();
        renderSites();
        renderDistricts();
        renderAttacks();
        renderCoevents();
        renderEvents();
        renderSelected();
        renderHighlighted();
    }

    /**
     * Expose only relevant functions
     */
    return {
        update,
        render
    };
}
