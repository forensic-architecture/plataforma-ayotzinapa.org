export default class Scene {

    constructor(url, renderer, locations ) {
        this.url = url;
        this.bbox = null;
        this.selectedEvents = null;
        this.data = null;
        this.labelsLoader = new Loader();
        this.actorCollapseTimerIds = [];
        this.loaders = {
            "scene": new Loader(),
            "stage": { "pclmesh": new POPLoader(), "context": new POPLoader() },
            "actor": { "opaque": new POPLoader(), "transparent": new POPLoader() },
        };
        this.renderer = renderer;
        this.popgeometry = { "stage": { "pclmesh": null, "context": null }, "actor": { "opaque": null, "transparent": null } };
        this.pois = [];
        this.labels = [];
        this.locations = locations;
        this.keyframe = 0;
        this.popLevelsStage = 0;
        this.popLevelsActor = 0;
        this.stageLevel = 0;
        this.actorLevel = 0;
        this.loadingCb = null;
        this.onKeyDown = ( event ) => {
            var key = event.key;
            if( key === 'b' ) {
                if( this.popgeometry.stage.pclmesh ) {
                    this.popgeometry.stage.pclmesh.toggleMonochrome();
                }
            }
            else {
                if( key in this.data._links.actors.opaque || key in this.data._links.actors.transparent ) 
                    this.reload(key);
            }
        }
        this.load(this.url);
        window.addEventListener(
            'keydown',
            this.onKeyDown,
            false
        );
    }

    setLoadingCallback( cb ) {
        this.loadingCb = cb;
    }

    updateLoadingPct( stageLevel, actorLevel ) {
        var pct = ( stageLevel + actorLevel ) /  ( this.popLevelsStage + this.popLevelsActor );
        if( this.loadingCb ) {
            this.loadingCb( pct );
        }
    }

    createPopGeometry( json ) {
        var popgeometry = new PopGeometry( this.renderer.getContext(), this.renderer.render );
        popgeometry.init( json );
        return popgeometry;
    }

    createPopStage( json ) {
        var stage = this.createPopGeometry( json );
        stage.enableMonochrome( false );
        this.renderer.addPopGeometry( stage, true, 'STAGE' );
        stage.getGeometry().renderOrder = 2;
        stage.setAlpha( 0.4 );
        stage.enableFog();
        return stage;
    }

    createPopContext( json ) {
        var context = this.createPopGeometry( json );
        this.renderer.addPopGeometry( context, true, 'CONTEXT' );
        context.getGeometry().renderOrder = 1;
        context.getGeometry().userData.maxAlpha = json.alpha;
        context.setAlpha( json.alpha );
        context.setFogColor( new THREE.Color( .3, .3, .3 ) );
        return context;
    }

    createPopActor( json, transparent = false ) {
        var actor = this.createPopGeometry( json );
        if( transparent ) {
            actor.getGeometry().renderOrder = 3;
        }
        this.renderer.addPopGeometry( actor, transparent, 'ACTOR' );
        actor.enableFog( true );
        actor.setFogDensity( .0015 );
        return actor;
    }

    load(url) {
        this.loaders.scene.full(
                url + "scene.json",
                "json",
                (response) => {
                    this.data = response;

                    this.renderer.init(this.data);
                    // Actor labels
                    this.labelsLoader.full(
                            url + "labels.json",
                            "json",
                            ( response ) => {
                                for( var obj in response ) {
                                    var label = new Label( response[obj], this.keyframe+1 ); // this.keyframe is 0-indexed while the incoming is 1-indexed.
                                    this.labels.push( label );
                                    this.renderer.addLabel( label );
                                }
                    });

                    if( this.data._links.actors.opaque.length > 0 ) {
                        // actor opaque
                        this.loaders.actor.opaque.load(
                                this.url + this.data._links.actors.opaque[0].href,
                                (json) => {
                                    this.popgeometry.actor.opaque = this.createPopActor( json );
                                    this.popLevelsActor += json.levels.length;
                                },
                                (data, level) => {
                                    if( this.popgeometry.actor.opaque ) {
                                        this.popgeometry.actor.opaque.updateData(data, level);
                                    }
                                    this.actorLevel++;
                                    this.updateLoadingPct( this.stageLevel, this.actorLevel );
                                }
                        );
                    }
                    
                    if( this.data._links.actors.transparent.length > 0 ) {
                        // actor transparent
                        this.loaders.actor.transparent.load(
                                this.url + this.data._links.actors.transparent[0].href,
                                (json) => {
                                    var transparent = true;
                                    this.popgeometry.actor.transparent = this.createPopActor( json, transparent );
                                    this.popLevelsActor += json.levels.length;
                                },
                                (data, level) => {
                                    if( this.popgeometry.actor.transparent ) {
                                        this.popgeometry.actor.transparent.updateData(data, level);
                                    }
                                    this.actorLevel++;
                                    this.updateLoadingPct( this.stageLevel, this.actorLevel );
                                }
                        );
                    }
                    
                    // stage
                    this.loaders.stage.pclmesh.load(
                            this.url + this.data._links.stage.pclmesh.href,
                            (json) => {
                                this.popgeometry.stage.pclmesh = this.createPopStage( json );
                                this.popLevelsStage += json.levels.length;
                            },
                            (data, level) => {
                                if( this.popgeometry.stage.pclmesh ) {
                                    this.popgeometry.stage.pclmesh.updateData(data, level);
                                }
                                this.stageLevel++;
                                this.updateLoadingPct( this.stageLevel, this.actorLevel );
                            }
                    );

                    // context
                    this.loaders.stage.context.load( 
                            this.url + this.data._links.stage.context.href,
                            (json) => {
                                this.popgeometry.stage.context = this.createPopContext( json );
                                this.popLevelsStage += json.levels.length;
                            },
                            (data, level) => {
                                if( this.popgeometry.stage.context ) {
                                    this.popgeometry.stage.context.updateData( data, level ); 
                                }
                                this.stageLevel++;
                                this.updateLoadingPct( this.stageLevel, this.actorLevel );
                            }
                    );
                    this.bbox = response.bbox;
                    this.createPoisForLocations();
                }
        );
    } 

    createPoisForLocations() {
        // pois
        for( var i = 0; i < this.locations.length; i++ ) {
            var poi = new Poi( this.locations[i], this.bbox );
            this.pois.push( poi );
            this.renderer.addPoi( poi );
            if( this.selectedEvents && this.selectedEvents.length > 0 ) {
                var locationId = this.selectedEvents[0].location.id;
                if( locationId === poi.metadata.id ) {
                    this.renderer.markPoiAsSelected( poi );
                }
            }
        }
    }

    updatePois( locations ) {
        this.locations = locations;
        // Start hidden.
        this.hidePois();
        // If we have new locations..
        if( this.locations.length > 0 ) {
            // Go through all pois and check if they already exist as locations.
            // If they exist update only the metadata and make them visible.
            for( var i = 0; i < this.pois.length; i++ ) {
                var locationIndex = this.locations.length;
                while( locationIndex-- ) { // This way so that iterators stay valid while splicing in a loop.
                    if( this.pois[i].metadata.id === this.locations[locationIndex].id ) {
                        this.pois[i].updateMetadata( this.locations[locationIndex] ); // Update events.
                        this.pois[i].show(); // Make this poi visible.
                        var locationIndex = this.locations.indexOf( locations[locationIndex] );
                        if( locationIndex > -1 ) { 
                            this.locations.splice( locationIndex, 1 ); // keep only the locations that still do not have a poi associated.
                        }
                    }
                }
            }

        }
        // We been through all our poi's so if we have a previously open/selected poi that
        // has no events for the currently applied filters ( meaning that its not visible ), reset it.
        if( this.renderer.getSelectedPoi() ) {
            if( ! this.renderer.getSelectedPoi().isVisible() ) {
                this.resetSelected();
            }
        }
        // Creat Poi's for all locations that do not yet have one created.
        this.createPoisForLocations();
        // Advance render loop.
        this.renderer.render();
    }

    resetSelected() {
        this.renderer.resetSelectedPoi();
        this.selectedEvents = [];
        this.selectedEvents = null;
    }

    updateSelected( selected ) {
        if( this.selectedEvents !== selected ) this.selectedEvents = selected;
    }

    hidePois() {
        for( var i = 0; i < this.pois.length; i++ ) {
            this.pois[i].hide();
        }
    }

    reloadActor( actor_dsc ) {
        actor_dsc.loader.load(
                this.url + actor_dsc.url,
                ( json ) => {
                    this.actorLevel = 0;
                    this.popLevelsActor = json.levels.length;
                    if( actor_dsc.popgeometry ) {
                        this.renderer.removeActor( actor_dsc.popgeometry.getGeometry() );
                    }
                    actor_dsc.popgeometry.init( json );
                    if( actor_dsc.transparent ) {
                        actor_dsc.popgeometry.getGeometry().renderOrder = 3;
                    }
                    this.renderer.addPopGeometry( actor_dsc.popgeometry, actor_dsc.transparent, 'ACTOR' );
                    actor_dsc.popgeometry.enableFog( true );
                    actor_dsc.popgeometry.setFogDensity( .0015 );
                },
                ( data, level ) => {
                    if( actor_dsc.popgeometry ) {
                        actor_dsc.popgeometry.updateData( data, level );
                    }
                    this.actorLevel++;
                    this.updateLoadingPct( this.stageLevel, this.actorLevel );
                });
    }

    getActorDescription( popgeometry, loader, url, transparent = false ) {
        return {
            'popgeometry': popgeometry,
                'loader': loader,
                'url': url,
                'transparent': transparent
        }
    }

    clearActorCollapseIdTimers() {
        this.actorCollapseTimerIds.forEach( ( id ) => {
            clearTimeout( id );
        });
        this.actorCollapseTimerIds = [];
    }

    collapseActor( actor, finishCb ) {
        this.clearActorCollapseIdTimers();
        var loadLevel = ( level, timerOffset ) => {
            var id = setTimeout( () => {
                actor.setLevel( level );
                if( level <= 0 ) {
                    this.renderer.removeActor( actor );
                    finishCb();
                }
            }, 50 * timerOffset );
            this.actorCollapseTimerIds.push( id );
        }
        if( actor ) {
            var currentLevel = actor.getLevel();
            for( var i = currentLevel - 1; i >= 0; i-- ) {
                loadLevel( i, currentLevel - i );
            }
        }
    }

    reload( keyframe ) {
        // Remove the current actors ( transparent / opaque ) and load the next one/s only if this keyframe is different from the previous one.
        if( this.keyframe !== keyframe ) {
            this.keyframe = keyframe;
            if( keyframe in this.data._links.actors.opaque ) {
                // opaque actor
                var reloadOpaqueCb = () => {
                    // Show only relevant labels for this keyframe.
                    this.labels.forEach( ( label ) => {
                        var labelKeyframe = label.getKeyframe() - 1;
                        if( labelKeyframe == this.keyframe ) label.show();
                        else label.hide();
                    });
                    this.reloadActor( this.getActorDescription( 
                                this.popgeometry.actor.opaque
                                , this.loaders.actor.opaque
                                , this.data._links.actors.opaque[ this.keyframe ].href
                                ) );
                }
                if( this.popgeometry.actor.opaque ) {
                    this.collapseActor( this.popgeometry.actor.opaque, reloadOpaqueCb );
                }
            }

            if( keyframe in this.data._links.actors.transparent ) {
                // transparent actor
                var reloadTransparentCb = () => {
                    this.reloadActor( this.getActorDescription( 
                                this.popgeometry.actor.transparent
                                , this.loaders.actor.transparent
                                , this.data._links.actors.transparent[ keyframe ].href
                                , true ) );
                }
                if( this.popgeometry.actor.transparent ) {
                    this.collapseActor( this.popgeometry.actor.transparent, reloadTransparentCb );
                }
            }

            if( this.selectedEvents && this.selectedEvents.length > 0 )
                this.renderer.selectCb( [] );
            this.resetSelected();
        }
    }

    abortLoading() {
        this.loaders.scene.abort();
        this.loaders.stage.pclmesh.abort();
        this.loaders.stage.context.abort();
        this.loaders.actor.opaque.abort();
        this.loaders.actor.transparent.abort();
    }

    dispose() {
        this.abortLoading();
        this.renderer.dispose();
        this.pois = [];
        this.locations = [];
        this.labels = [];
        delete this.popgeometry.actor.opaque;
        this.popgeometry.actor.opaque = null;
        delete this.popgeometry.actor.transparent;
        this.popgeometry.actor.transparent = null;
        delete this.popgeometry.stage.pclmesh;
        this.popgeometry.stage.pclmesh = null;
        delete this.popgeometry.stage.context;
        this.popgeometry.stage.context = null;
        this.clearActorCollapseIdTimers();
        window.removeEventListener( 
            'keydown',
            this.onKeyDown,
            false
        );
    }
}

