import React from 'react';

import copy from '../js/data/copy.json';

export default class KeyframeInfo extends React.Component {
  goToNextKeyFrame() {
    if (this.props.keyframe < this.props.keyframes[this.props.district].length - 1) {
        this.props.filter({ keyframe: this.props.keyframe + 1 });
    }
  }

  goToPrevKeyFrame() {
    if (this.props.keyframe > 0) {
        this.props.filter({ keyframe: this.props.keyframe - 1 });
    }
  }

  render() {
    const keyframe = this.props.keyframe;
    const keyframes = this.props.keyframes;
    const district = this.props.district;
    const language = this.props.language;
    if (!this.props.isView2d) {
        const texts = copy[language].keyframes[keyframes[district][keyframe].id];
        return (<div className="keyframe-info">
          <h6>{`${keyframe + 1} / ${keyframes[district].length} .`}</h6>
          {
            texts.map(text => {
                if (text.type === 'h3') return (<h3>{text.text}</h3>);
                return (<p>{text.text}</p>);
            })
          }
          <div className="actions">
            <div className={`${(!keyframe) ? 'disabled ' : ''} action`} onClick={() => this.goToPrevKeyFrame()}>&larr;</div>
            <div className={`${(keyframe >= keyframes[district].length - 1) ? 'disabled ' : ''} action`} onClick={() => this.goToNextKeyFrame()}>&rarr;</div>
          </div>
        </div>);
    }
    return (<div/>)
    }
};
