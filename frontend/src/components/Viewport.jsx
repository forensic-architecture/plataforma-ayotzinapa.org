import '../scss/main.scss';

import React from 'react';

import View2D from './View2D.jsx';
import View3D from './View3D.jsx';
import Renderer from '../../../app3d/static/app3d/js/renderer.js'

class Viewport extends React.Component {

    constructor(props) {
        super(props);
        if (this.props.app.isWebGL) {
          this.renderer = new Renderer(this.props.select);
        }
    }

    render() {
        if( this.props.ui.flags.isView2d ) {
            return (
                <View2D
                    locations={this.props.domain.locations.filter(item => item)}
                    coevents={this.props.domain.coevents.filter(item => item)}
                    attacks={this.props.domain.attacks.filter(item => item)}
                    routes={this.props.domain.routes}
                    sites={this.props.domain.sites}
                    views={this.props.app.filters.views}
                    selected={this.props.app.selected}
                    highlighted={this.props.app.highlighted}
                    colors={this.props.ui.style.colors}
                    dom={this.props.ui.dom}
                    select={this.props.select}
                    highlight={this.props.highlight}
                    isActive={this.props.ui.flags.isView2d}
                    activateDistrict={this.props.activateDistrict}
                />
            );
        }
        else {
            return(
                <View3D
                    base_uri={this.props.app.base_uri}
                    base_district_uri={this.props.app.district.base_uri}
                    districtId={this.props.app.district.id}
                    districtName={this.props.app.district.name}
                    selected={this.props.app.selected}
                    locations={this.props.domain.locations}
                    keyframe={this.props.app.filters.keyframe}
                    renderer={this.renderer}
                    toggle={this.props.toggle}
                />
            );
        }
    }
}

export default Viewport;
