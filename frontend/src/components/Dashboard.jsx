import '../scss/main.scss';

import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';
import * as selectors from '../selectors';

import Cabinet from './Cabinet.jsx';
import LoadingOverlay from './LoadingOverlay.jsx';
import Viewport from './Viewport.jsx';
import Toolbar from './Toolbar.jsx';
import CardStack from './CardStack.jsx';
import InfoPopUp from './InfoPopup.jsx';
import TimeRack from './TimeRack.jsx';
import KeyframeInfo from './KeyframeInfo.jsx';

class Dashboard extends React.Component {

    constructor(props) {
        super(props);

        this.handleHighlight = this.handleHighlight.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleDistrict = this.handleDistrict.bind( this );
        // District name by id.
        this.districtNamesById = [];
        this.districtNamesById['7'] = 'pj';
        this.districtNamesById['8'] = 'st';
        this.districtNamesById['9'] = 'ja';
    }

    componentDidMount() {
        if (!this.props.app.isMobile) {
            this.props.actions.fetchDomain()
                .then((domain) => this.props.actions.updateDomain(domain));
        }
    }

    handleHighlight(highlighted) {
        this.props.actions.updateHighlighted((highlighted) ? highlighted : null);
    }

    handleSelect(selected) {
        if (selected) {
            // attacks are not susceptible to tag filters, so make sure this happens only when they are found
            // in the domain
            let eventsToSelect = selected.map(eventId => this.props.domain.events[eventId]);
            eventsToSelect = eventsToSelect.sort((a, b) => {
              return this.props.ui.tools.parser(a.timestamp) - this.props.ui.tools.parser(b.timestamp);
            });

            if (eventsToSelect.every(event => (event))) {
                this.props.actions.updateSelected(eventsToSelect);
            }

            // Now fetch detail data for each event
            // Add transmitter and receiver data for coevents
            this.props.actions.fetchEvents(selected)
                .then((events) => {
                    let eventsSelected = events.map(ev => {
                        const event = Object.assign({}, ev, this.props.domain.events[ev.id]);
                        event.narrative = Object.assign({}, this.props.domain.tags.byId[ev.narrative]);

                        const coevent = this.props.domain.coevents[ev.id];
                        if (coevent) return Object.assign({}, event, coevent);

                        const attack = this.props.domain.attacks[ev.id];
                        if (attack) return Object.assign({}, event, attack.event);

                        return event;
                    });
                    eventsSelected = eventsSelected.sort((a, b) => {
                      return this.props.ui.tools.parser(a.timestamp) - this.props.ui.tools.parser(b.timestamp);
                    });
                    this.props.actions.updateSelected(eventsSelected);
                });
        } else {
            this.props.actions.updateSelected([]);
        }
    }

    handleFilter(filter) {
        this.props.actions.updateFilters(filter);
    }

    handleDistrict( districtId ) {
        if( ( this.props.domain.districts.allIds.indexOf( districtId ) > -1 ) ) {
            this.props.actions.updateDistrict( this.fillDistrict( districtId ) );
            this.props.actions.updateFilters( {keyframe: 0} );
            if( this.props.ui.flags.isView2d ) { // If we are in 2d toggle the view.
                this.props.actions.toggleView();
            }
        }
    }

    handleToggle( key ) {
        switch( key ) {
            case 'TOGGLE_VIEW':
                {
                    if( ! this.props.ui.flags.isView2d ) { // Here we toggle only to 2d. handleDistrict takes care of 3d.
                        this.resetDistrict();
                        this.props.actions.toggleView();
                        this.props.actions.updateFilters( {keyframe: null} );
                    }
                    break;
                }
            case 'TOGGLE_CARDSTACK':
                {
                    this.props.actions.updateSelected([]);
                    break;
                }
            case 'TOGGLE_INFOPOPUP':
                {
                    this.props.actions.toggleInfoPopup();
                    break;
                }
        }
    }

    resetDistrict() {
        var district = { id: -1, name: '', base_uri: this.props.app.district.base_uri };
        this.props.actions.updateDistrict( district );
    }

    fillDistrict( districtId ) {
        // Create a district with the new data.
        var locations = this.getLocationsForDistrict( districtId );
        var district = { id: districtId, name: this.districtNamesById[ districtId ], base_uri: this.props.app.district.base_uri };
        return district;
    }

    getLocationsForDistrict( districtId ) {
        var locations = []
            // Get all location id's for the current district ( aka scene ).
            var districtLocationsIds = this.props.domain.districts.byId[districtId].locations;
        districtLocationsIds.forEach(( locationId ) => {
            // Find the current location in locations.
            if( locationId in this.props.domain.locations ) {
                // Save the location ( including the associated events ) for easy access on select.
                locations.push( this.props.domain.locations.byId[ locationId ] );
            }
        });
        return locations;
    }

    renderCabinet() {
        return (
          <Cabinet
            handleDistrict={this.handleDistrict}
          />);
    }

    renderTool() {

        return (<div>
            <Viewport
                domain={this.props.domain}
                app={this.props.app}
                ui={this.props.ui}
                select={this.handleSelect}
                filter={this.handleFilter}
                highlight={this.handleHighlight}
                toggle={(key) => this.handleToggle(key)}
                activateDistrict={this.handleDistrict}
            />
            <Toolbar
                isToolbar={this.props.ui.flags.isToolbar}
                toolbarTab={this.props.ui.components.toolbarTab}
                isView2d={this.props.ui.flags.isView2d}
                language={this.props.app.language}
                tags={this.props.domain.tags}
                tagTypes={this.props.domain.tag_types}
                tagGroups={this.props.domain.tag_groups}
                tagSubgroups={this.props.domain.tag_subgroups}
                narratives={this.props.domain.narratives}
                tagFilters={this.props.app.filters.tags}
                viewFilters={this.props.app.filters.views}
                narrativeFilters={this.props.app.filters.narratives}
                filter={this.handleFilter}
                toggle={ (key) => this.handleToggle(key) }
                activateDistrict={this.handleDistrict}
                actions={this.props.actions}
            />
            <CardStack
                tags={this.props.domain.tags}
                selected={this.props.app.selected}
                language={this.props.app.language}
                tools={this.props.ui.tools}
                isCardstack={this.props.ui.flags.isCardstack}
                isFetchingEvents={this.props.ui.flags.isFetchingEvents}
                highlight={this.handleHighlight}
                filter={this.handleFilter}
                toggle={this.handleToggle}
            />
            <TimeRack
                domain={this.props.domain}
                app={this.props.app}
                ui={this.props.ui}
                select={this.handleSelect}
                filter={this.handleFilter}
                highlight={this.handleHighlight}
                toggle={() => this.handleToggle('TOGGLE_CARDSTACK')}
            />
            <InfoPopUp
                ui={this.props.ui}
                app={this.props.app}
                toggle={() => this.handleToggle('TOGGLE_INFOPOPUP')}
            />
            <KeyframeInfo
                isView2d={this.props.ui.flags.isView2d}
                keyframe={this.props.app.filters.keyframe}
                keyframes={this.props.app.keyframes}
                district={this.props.app.district.name}
                language={this.props.app.language}
                filter={this.handleFilter}
            />
            <LoadingOverlay
                ui={this.props.ui}
                language={this.props.app.language}
            />
        </div>
      )
    }

    render() {
        if (this.props.ui.flags.isCabinet) {
            return (<div>{this.renderCabinet()}</div>);
        } else {
            return (<div>{this.renderTool()}</div>);
        }
    }
}

function mapStateToProps(state) {
    return Object.assign({}, state, {
        domain: Object.assign({}, state.domain, {
            events: selectors.getFilteredEvents(state),
            attacks: selectors.getFilteredAttacks(state),
            coevents: selectors.getFilteredCoevents(state),
            //districts: selectors.getFilteredDistricts(state),
            locations: selectors.getFilteredLocations(state),
            narratives: selectors.getFilteredNarratives(state),
            routes: selectors.getFilteredRoutes(state),
            sites: selectors.getFilteredSites(state),
            tag_groups: selectors.getFilteredTagGroups(state),
            tag_subgroups: selectors.getFilteredTagSubgroups(state),
            tag_types: selectors.getFilteredTagTypes(state),
            tags: selectors.getFilteredTags(state),
        }),
        app: Object.assign({}, state.app, {
            filters: Object.assign({}, state.app.filters, {
                range: selectors.getRangeFilter(state),
            })
        }),
        ui: state.ui
    });
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(
        mapStateToProps,
        mapDispatchToProps,
        )(Dashboard);
