import Tree from './tree';

export default function Detail(App) {

  let selected;

  d3.select("#detail-background")
    .on("click", function () {
      d3.select(this.parentNode).classed("hidden", true);
    });

  function show() {
    d3.select("#detail-wrapper")
      .classed("hidden", false);
  }

  function hide() {
    d3.select("#detail-wrapper")
      .classed("hidden", true);

    d3.select("#detail-wrapper svg").remove();
  }

  /**
  * Given a tag ID, look for its associated role and the entire hierarchy tree
  * @param {number} tag_id: tag_id associated with person clicked
  */
  function loadTagHierarchy(tag_id) {
    d3.json(`api/roles/${tag_id}?format=json`, (json) => {
      if (json.length > 0) {
        renderTree(json, tag_id);
      }
    });
  }

  function update() {

  }

  /**
  * Render hierarchy tree using d3 tree structure
  * @param {object} data: tree data tructure with hierarchy to display
  * @param {number} tag_id: tag ID to highlight in the tree hierarchy
  */
  function renderTree(data, tag_id) {
    const treeData = data[0];

    const tree = new Tree(App, {
      svgID: "#svg",
      data: data[0],
      tag_ID: tag_id
    });
    tree.render();
  }

  /**
  * Retrieves all events for a given tag and lists them
  * @param {number} tag_id: tag ID to highlight in the tree hierarchy
  */
  function renderEvents(tag_id) {
    // TODO: Do that
  }

  /**
  * Renders title and overview for popUp
  * @param {object} datapoint: oboject that contains the downdiggable data
  */
  function updateTitle(tag) {
    d3.select("#summary-title")
      .html(tag.name)
  }

  /**
  * Loads the hierarchy tree data
  * @param {number} tag_id: tag ID to highlight in the tree hierarchy
  */
  function render(tag) {
    loadTagHierarchy(tag.id);
    updateTitle(tag);
    show();
  }

  return {
    show,
    hide,
    update,
    render
  }
}
